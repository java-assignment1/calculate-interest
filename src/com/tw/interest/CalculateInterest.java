package com.tw.interest;

public class CalculateInterest {

    private final String accountType;
    private int balance;
    private float interest = 0;

    public CalculateInterest(String accountType, int balance) {
        this.accountType = accountType;
        this.balance = balance;
    }

    public void deposit(int amount) {
        this.balance += amount;
    }

    public double calculateInterest() {
        if (this.accountType.equals("savings")) {
            int rateOfInterest = 3;
            float time = 0.25F;
            interest = ((this.balance) * rateOfInterest * time) / 100;
        }
        return interest;
    }

    public void withdraw(int amount) {
        this.balance -= amount;
    }

}
