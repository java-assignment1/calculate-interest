package com.tw.interest;

public class BharatBank {

    public static void main(String[] args) {
        CalculateInterest amrita = new CalculateInterest("savings", 100000);
        CalculateInterest gopal = new CalculateInterest("current", 50000);

        gopal.deposit(10000);

        System.out.println(amrita.calculateInterest());
        System.out.println(gopal.calculateInterest());

        amrita.withdraw(45000);
    }
}
