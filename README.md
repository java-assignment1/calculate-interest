Gopal and Amrita are the customers of Bharat Bank. Gopal is holding a current account and Amrita is holding a savings account.

Gopal had a balance of 50,000 rupees and did a deposit of 10,000 rupees additionally.(Assume it was on Jan 1st)
Amrita was having a balance of 1,00,000 and she withdrew 45,000 rupees.(Assume it was on 31st March)
The interest rates are for savings 3% p.a. and for current account 0% p.a.

Calculate the interest for the period of January 2021 - March 2021 Quarter. A point to note: Gopal has contacted the bank today to withdraw 70,000 rupees.